﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TdCacheVersionControlLib.Reporting;
using TdCacheVersionControlLib.Scanner;

namespace TdCacheVersionControlLib {
    public class CacheVersionControlMaker {
        public CacheVersionControlMaker() {
            this.cacheControlData = new CacheControlData();
        }
        public void buildCacheForSite() {
            CacheDirectoryScanner cds = new CacheDirectoryScanner(this.cacheControlData.webSiteDir, this.cacheControlData);
            cds.scanDirectory();
            HtmlCacheReport hcr = new HtmlCacheReport(this.cacheControlData);
            hcr.writeReport();
            JsonCacheReport jcr = new JsonCacheReport(this.cacheControlData);
            jcr.writeReport();
            logger.Info("Build for cache complete.");
            int junk = 1;
        }
        public CacheControlData cacheControlData { get; private set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();
    }
}
