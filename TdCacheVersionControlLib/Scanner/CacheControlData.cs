﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdCacheVersionControlLib.Scanner {
    public class CacheControlData {
        public CacheControlData() {
            this.webSiteDir = ConfigurationManager.AppSettings["webSiteDir"];
            this.cacheControlReportsDir = ConfigurationManager.AppSettings["cacheControlReportsDir"];
            this.htmlFileToCacheDict = new Dictionary<string, HtmlScanFile>();
            this.jsFileToCacheDict = new Dictionary<string, JsScanFile>();
        }
        public string webSiteDir { get; set; }
        public string cacheControlReportsDir { get; set; }
        public Dictionary<string, HtmlScanFile> htmlFileToCacheDict { get; set; }
        public Dictionary<string, JsScanFile> jsFileToCacheDict { get; set; }
    }
}
