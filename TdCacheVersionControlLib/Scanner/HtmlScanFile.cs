﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TdCacheVersionControlLib.Scanner {
    public class HtmlScanFile {
        public HtmlScanFile(string file_path) {
            this.filePath = file_path;
        }
        public void scanFile() {
            String s = File.ReadAllText(this.filePath);
            this.fileSize = Math.Round((Double)s.Length / 1000D, 2);
            //find all the .js files
            Match js_m = Regex.Match(s, "<script.*?\\.js.*?>");
            while (js_m.Success) {
                //there's usually just ONE group here.
                foreach (Group g in js_m.Groups) {
                    string in_js_s = g.Value;
                    string out_js_s = this.makeOutJsString(in_js_s);
                    if (out_js_s != null) {
                        s = s.Replace(in_js_s, out_js_s);
                    }
                    this.jsFilesTotal++;

                }
                js_m = js_m.NextMatch();
            }
            //find all the .css files
            Match css_m = Regex.Match(s, "<link.*?\\.css.*?>");
            while (css_m.Success) {
                //there's usually just ONE group here.
                foreach (Group g in css_m.Groups) {
                    string in_css_s = g.Value;
                    string out_css_s = this.makeOutCssString(in_css_s);
                    if (out_css_s != null) {
                        s = s.Replace(in_css_s, out_css_s);
                    }
                    this.cssFilesTotal++;


                }
                css_m = css_m.NextMatch();
            }
            if (this.needsFileSave) {
                File.WriteAllText(this.filePath, s);
            }
        }
        /// <summary>
        /// Puts vx (version entry) in against this javascript source string.
        /// If null is returned, NOTHING has changed (no reason to update anything).
        /// If string is returned, we have a new vx entry.
        /// Important to understand that versioning here is based on string HASH, not file creation time. This is crude, but likely pretty good, and it keeps moving the files (and thus setting a new creation date) from corrupting things.
        /// </summary>
        /// <param name="in_js_s"></param>
        /// <returns></returns>
        private string makeOutJsString(string in_js_s) {
            String rs = null;
            //get src_s built, which is the .js entry WITHOUT quotes or the other HTML stuff...
            int start = in_js_s.IndexOf("src");
            string junk_s = in_js_s.Substring(start + 3);
            string[] junk_A = junk_s.Split(new char[] { '\"', '\'' });
            string src_s = junk_A[1];
            //set up current_version. It will either be 0 (unused yet), or have an old hashcode entry
            int current_version = 0;
            if (src_s.Contains("?vx=")) {
                string[] version_A = src_s.Split(new char[] { '?' });
                src_s = version_A[0];
                current_version = int.Parse(version_A[1].Replace("vx=", ""));
            }
            //gotta go look at the js_file itself to get the hashcode, and compare this to current version
            string js_file_path = Path.GetDirectoryName(this.filePath) + @"\\" + src_s.Replace("/", "\\");
            if (File.Exists(js_file_path)) {
                int the_hash = File.ReadAllText(js_file_path).GetHashCode();
                if (current_version != the_hash) {
                    string swap_src_s = src_s + "?vx=" + the_hash + "";
                    string src_n = in_js_s;
                    if (src_n.IndexOf('?') > -1)
                        src_n = (src_n.Split(new char[] { '?' }))[0].ToString() + "\">";
                    rs = src_n.Replace(src_s, swap_src_s);
                    this.jsFilesCacheUpdateTotal++;
                    this.needsFileSave = true;
                }
            } else {
                this.jsFilesBadLinksTotal++;
            }
            return rs;

        }
        /// <summary>
        /// Puts vx (version entry) in against this javascript source string.
        /// If null is returned, NOTHING has changed (no reason to update anything).
        /// If string is returned, we have a new vx entry.
        /// Important to understand that versioning here is based on string HASH, not file creation time. This is crude, but likely pretty good, and it keeps moving the files (and thus setting a new creation date) from corrupting things.
        /// </summary>
        /// <param name="in_js_s"></param>
        /// <returns></returns>
        private string makeOutCssString(string in_css_s) {
            String rs = null;
            //get src_s built, which is the .js entry WITHOUT quotes or the other HTML stuff...
            int start = in_css_s.IndexOf("href");
            string junk_s = in_css_s.Substring(start + 4);
            string[] junk_A = junk_s.Split(new char[] { '\"', '\'' });
            string src_s = junk_A[1];
            //set up current_version. It will either be 0 (unused yet), or have an old hashcode entry
            int current_version = 0;
            if (src_s.Contains("?vx=")) {
                string[] version_A = src_s.Split(new char[] { '?' });
                src_s = version_A[0];
                current_version = int.Parse(version_A[1].Replace("vx=", ""));
            }
            //gotta go look at the css_file itself to get the hashcode, and compare this to current version
            string css_file_path = Path.GetDirectoryName(this.filePath) + @"\\" + src_s.Replace("/", "\\");
            if (File.Exists(css_file_path)) {
                int the_hash = File.ReadAllText(css_file_path).GetHashCode();
                if (current_version != the_hash) {
                    string swap_src_s = src_s + "?vx=" + the_hash + "";
                    if (in_css_s.IndexOf("?vx=") > -1)
                    {
                        rs = in_css_s.Replace(src_s + "?vx=" + current_version, swap_src_s);
                    }
                    else
                    {
                        rs = in_css_s.Replace(src_s, swap_src_s);
                    }
                    this.cssFilesCacheUpdateTotal++;
                    this.needsFileSave = true;
                }
            } else {
                this.cssFilesBadLinksTotal++;
            }
            return rs;

        }
        public bool needsFileSave { get; private set; }
        public string filePath { get; private set; }
        public double fileSize { get; private set; }
        public int jsFilesTotal { get; private set; }
        public int jsFilesBadLinksTotal { get; private set; }
        public int jsFilesCacheUpdateTotal { get; private set; }
        public int cssFilesTotal { get; private set; }
        public int cssFilesBadLinksTotal { get; private set; }
        public int cssFilesCacheUpdateTotal { get; private set; }
        public int jsonFilesTotal { get; private set; }
        //public int jsonFilesBadLinksTotal { get; private set; }
        public int jsonFilesCacheUpdateTotal { get; private set; }
    }
}
