﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TdCacheVersionControlLib.Scanner
{
    public class JsScanFile
    {
        public JsScanFile(string file_path)
        {
            this.filePath = file_path;
            this.rootDirectoryPath = file_path.Substring(0, file_path.LastIndexOf("tdportal_jobmanagement") + 22);
        }

        public void scanFile()
        {
            String s = File.ReadAllText(this.filePath);
            this.fileSize = Math.Round((Double)s.Length / 1000D, 2);
            //find all the .json files
            //Match css_m = Regex.Match(s, "<link.*?\\.css.*?>");
            //Match json_m = Regex.Match(s, "$.getJSON(.*?\\, function (data)");
            //Match json_m = Regex.Match(s, "$.getJSON" + Regex.Escape("(") + ".*?, function " + Regex.Escape("(") + "data" + Regex.Escape(")") + "");
            Match json_m = Regex.Match(s, Regex.Escape("$.") + "getJSON" + Regex.Escape("(") + ".*?, function " + Regex.Escape("(") + "data" + Regex.Escape(")") + " ");
            while (json_m.Success)
            {
                //there's usually just ONE group here.
                foreach (Group g in json_m.Groups)
                {
                    string in_json_s = g.Value;
                    string out_json_s = this.makeOutJsonString(in_json_s);
                    if (out_json_s != null)
                    {
                        s = s.Replace(in_json_s, out_json_s);
                    }
                    this.jsonFilesTotal++;


                }
                json_m = json_m.NextMatch();
            }
            if (this.needsFileSave)
            {
                File.WriteAllText(this.filePath, s, Encoding.Unicode);
            }
        }
        /// <summary>
        /// Puts vx (version entry) in against this json source string.
        /// If null is returned, NOTHING has changed (no reason to update anything).
        /// Cannot get a hashcode for these entries easily (some are variables). So, we just generate a new vx (based on datetime stamp) EVERY time.
        /// A bit sloppy, but the static json here is likely pretty small in size across the entire site, so this does make a bit of sense.
        /// </summary>
        /// <param name="in_js_s"></param>
        /// <returns></returns>
        private string makeOutJsonString(string in_js_s)
        {
            String rs = null;
            //BY DEFINITION, our string will START with "$.getJSON", and end with " function (data) ".
            //this is important in how we assemble the vx string
            //get src_s built, which is the .js entry WITHOUT quotes or the other HTML stuff...
            string junk_s = in_js_s.Replace("$.getJSON(", "").Replace(", function (data) ", "");
            //remove any old vs codes (they will have a " + " here.
            int current_version = 0;
            if (junk_s.Contains("?vx="))
            {
                string[] version_A = junk_s.Split(new char[] { '?' });
                junk_s = version_A[0].Replace("\"", "");
                current_version = int.Parse(version_A[1].Replace("vx=", "").Replace("\"", ""));
            }

            string current_json_url = junk_s.Replace("\"", "").Replace("'", "");
            if (current_json_url.ToLower().IndexOf(".json") > -1)
            {
                string[] json_path_positions = current_json_url.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
                string pattern_to_match = json_path_positions[json_path_positions.Length - 1].IndexOf('?') > -1 ?
                                                        json_path_positions[json_path_positions.Length - 1].Substring(0, json_path_positions[json_path_positions.Length - 1].IndexOf('?'))
                                                      : json_path_positions[json_path_positions.Length - 1];
                string json_file_path = Directory.GetFiles(this.rootDirectoryPath, pattern_to_match, SearchOption.AllDirectories).First();
                if (File.Exists(json_file_path))
                {
                    int the_hash = File.ReadAllText(json_file_path).GetHashCode();
                    if (current_version != the_hash)
                    {
                        // rs = "$.getJSON(" + junk_s + "+\"?vx=" + the_hash + "\", function(data) ";
                        rs = "$.getJSON(\"" + current_json_url + "?vx=" + the_hash + "\", function (data) ";
                    }
                }
            }

            //if (junk_s.Contains(" + \"?vx=")) {
            //    junk_s = junk_s.Substring(0, junk_s.IndexOf("+ \"?vx=")).Trim();
            //}
            //rs = "$.getJSON(" + junk_s + "+\"?vx=" + current_version + "\", function(data) ";
            this.jsonFilesCacheUpdateTotal++;
            this.needsFileSave = true;
            return rs;

        }
        public bool needsFileSave { get; private set; }
        public string filePath { get; private set; }
        public string rootDirectoryPath { get; set; }
        public double fileSize { get; private set; }
        public int jsonFilesTotal { get; private set; }
        //public int jsonFilesBadLinksTotal { get; private set; }
        public int jsonFilesCacheUpdateTotal { get; private set; }
    }
}
