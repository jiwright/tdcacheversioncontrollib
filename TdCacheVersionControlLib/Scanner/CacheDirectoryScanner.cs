﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TdCacheVersionControlLib.Scanner
{
    public class CacheDirectoryScanner
    {
        public CacheDirectoryScanner(string base_dir, CacheControlData the_control_data)
        {
            this.baseDir = base_dir;
            this.cacheControlData = the_control_data;
        }
        public void scanDirectory()
        {
            //Scans all the js files in the site.
            List<string> js_file_list = Directory.GetFiles(this.baseDir, "*.js", SearchOption.AllDirectories).ToList();
            foreach (string js_file_path in js_file_list)
            {
                JsScanFile js_scan_file = new JsScanFile(js_file_path);
                js_scan_file.scanFile();
                this.cacheControlData.jsFileToCacheDict.Add(js_file_path, js_scan_file);
            }

            //Scans all the html files in the site.
            List<string> html_file_list = Directory.GetFiles(this.baseDir, "*.htm*", SearchOption.AllDirectories).ToList();
            foreach (string html_file_path in html_file_list)
            {
                HtmlScanFile scan_file = new HtmlScanFile(html_file_path);
                scan_file.scanFile();
                this.cacheControlData.htmlFileToCacheDict.Add(html_file_path, scan_file);
            }
            //List<string> dir_list = Directory.GetDirectories(this.baseDir).ToList();
            //foreach (String inner_dir in dir_list) {
            //    CacheDirectoryScanner cds = new CacheDirectoryScanner(inner_dir, this.cacheControlData);
            //    cds.scanDirectory();
            //}

        }
        public string baseDir { get; private set; }
        public CacheControlData cacheControlData { get; private set; }
    }
}
