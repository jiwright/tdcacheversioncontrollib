﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TdCacheVersionControlLib.Scanner;

namespace TdCacheVersionControlLib.Reporting {
    public class HtmlCacheReport {
        public HtmlCacheReport(CacheControlData cache_control_data) {
            this.cacheControlData = cache_control_data;
        }
        public void writeReport() {
            List<string> report_list = new List<string>();
            this.appendReportHeader(report_list);
            foreach (HtmlScanFile the_scan_file in this.cacheControlData.htmlFileToCacheDict.Values) {
                StringBuilder sb = new StringBuilder();
                sb.Append(the_scan_file.filePath.Replace(this.cacheControlData.webSiteDir, "") + "\t");
                sb.Append(the_scan_file.fileSize + "\t");
                sb.Append(the_scan_file.jsFilesTotal + "\t");
                sb.Append(the_scan_file.jsFilesBadLinksTotal + "\t");
                sb.Append(the_scan_file.jsFilesCacheUpdateTotal + "\t");
                sb.Append(the_scan_file.cssFilesTotal + "\t");
                sb.Append(the_scan_file.cssFilesBadLinksTotal + "\t");
                sb.Append(the_scan_file.cssFilesCacheUpdateTotal + "\t");
                report_list.Add(sb.ToString());
            }
            File.WriteAllLines(this.cacheControlData.cacheControlReportsDir + @"\cache_report.txt", report_list);
        }
        private void appendReportHeader(List<string> report_list) {
            StringBuilder sb = new StringBuilder();
            sb.Append("filePath" + "\t");
            sb.Append("fileSize" + "\t");
            sb.Append("jsFilesTotal" + "\t");
            sb.Append("jsFilesBadLinksTotal" + "\t");
            sb.Append("jsFilesCacheUpdateTotal" + "\t");
            sb.Append("cssFilesTotal" + "\t");
            sb.Append("cssFilesBadLinksTotal" + "\t");
            sb.Append("cssFilesCacheUpdateTotal" + "\t");
            report_list.Add(sb.ToString());
        }
        public CacheControlData cacheControlData { get; private set; }
    }
}
