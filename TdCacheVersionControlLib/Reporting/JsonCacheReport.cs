﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TdCacheVersionControlLib.Scanner;

namespace TdCacheVersionControlLib.Reporting {
    public class JsonCacheReport {
        public JsonCacheReport(CacheControlData cache_control_data) {
            this.cacheControlData = cache_control_data;
        }
        public void writeReport() {
            List<string> report_list = new List<string>();
            this.appendReportHeader(report_list);
            foreach (JsScanFile the_scan_file in this.cacheControlData.jsFileToCacheDict.Values) {
                StringBuilder sb = new StringBuilder();
                sb.Append(the_scan_file.filePath.Replace(this.cacheControlData.webSiteDir, "") + "\t");
                sb.Append(the_scan_file.fileSize + "\t");
                sb.Append(the_scan_file.jsonFilesTotal + "\t");
                //sb.Append(the_scan_file.jsonFilesBadLinksTotal + "\t");
                sb.Append(the_scan_file.jsonFilesCacheUpdateTotal);
                report_list.Add(sb.ToString());
            }
            File.WriteAllLines(this.cacheControlData.cacheControlReportsDir + @"\json_cache_report.txt", report_list);
        }
        private void appendReportHeader(List<string> report_list) {
            StringBuilder sb = new StringBuilder();
            sb.Append("filePath" + "\t");
            sb.Append("fileSize" + "\t");
            sb.Append("jsonFilesTotal" + "\t");
            //sb.Append("jsonFilesBadLinksTotal" + "\t");
            sb.Append("jsonFilesCacheUpdateTotal");
            report_list.Add(sb.ToString());
        }
        public CacheControlData cacheControlData { get; private set; }
    }
}
